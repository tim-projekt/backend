package pl.wypozyczalnia.tim.wypozyczalnia.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Vehicle;

public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
}
