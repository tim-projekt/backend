package pl.wypozyczalnia.tim.wypozyczalnia.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.wypozyczalnia.tim.wypozyczalnia.models.CarRent;

public interface CarRentRepository extends CrudRepository<CarRent, Long> {
}
