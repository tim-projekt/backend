package pl.wypozyczalnia.tim.wypozyczalnia.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Report;

public interface ReportRepository extends CrudRepository<Report, Long> {
}
