package pl.wypozyczalnia.tim.wypozyczalnia.services;

import pl.wypozyczalnia.tim.wypozyczalnia.models.Report;

import java.util.List;

public interface ReportService {
    Report getReport(Long id);
    List<Report> getUserReports();
    Report createReport(Report report);
    Report updateReport(Long id, Report report);
}
