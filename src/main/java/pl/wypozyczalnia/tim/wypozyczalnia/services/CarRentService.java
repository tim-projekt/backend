package pl.wypozyczalnia.tim.wypozyczalnia.services;

import pl.wypozyczalnia.tim.wypozyczalnia.models.CarRent;

import java.util.List;

public interface CarRentService {
    List<CarRent> getCarHires();
    CarRent getCarHire(Long id);
    CarRent createCarHire(CarRent carRent);
    CarRent updateCarHire(Long id, CarRent carRent);
}
