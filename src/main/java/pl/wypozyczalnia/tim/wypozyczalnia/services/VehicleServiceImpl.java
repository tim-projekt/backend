package pl.wypozyczalnia.tim.wypozyczalnia.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Vehicle;
import pl.wypozyczalnia.tim.wypozyczalnia.repositories.VehicleRepository;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {


    private VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }


    // todo: implement functions
    @Override
    public List<Vehicle> getVehicles() {
        return null;
    }

    @Override
    public Vehicle createVehicle(Vehicle vehicle) {
        return null;
    }

    @Override
    public Vehicle getVehicle(Long id) {
        return null;
    }

    @Override
    public Vehicle updateVehicle(Long id, Vehicle vehicle) {
        return null;
    }
}
