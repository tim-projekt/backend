package pl.wypozyczalnia.tim.wypozyczalnia.services;

import pl.wypozyczalnia.tim.wypozyczalnia.models.Vehicle;

import java.util.List;

public interface VehicleService {
    List<Vehicle> getVehicles();

    Vehicle createVehicle(Vehicle vehicle);

    Vehicle getVehicle(Long id);

    Vehicle updateVehicle(Long id, Vehicle vehicle);
}
