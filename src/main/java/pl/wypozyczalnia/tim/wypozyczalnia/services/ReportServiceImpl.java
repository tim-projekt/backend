package pl.wypozyczalnia.tim.wypozyczalnia.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Report;
import pl.wypozyczalnia.tim.wypozyczalnia.repositories.ReportRepository;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private ReportRepository reportRepository;

    @Autowired
    public ReportServiceImpl(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    // todo: implement functions

    @Override
    public Report getReport(Long id) {
        return null;
    }

    @Override
    public List<Report> getUserReports() {
        return null;
    }

    @Override
    public Report createReport(Report report) {
        return null;
    }

    @Override
    public Report updateReport(Long id, Report report) {
        return null;
    }
}
