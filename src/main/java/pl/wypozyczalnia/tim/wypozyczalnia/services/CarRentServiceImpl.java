package pl.wypozyczalnia.tim.wypozyczalnia.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wypozyczalnia.tim.wypozyczalnia.models.CarRent;
import pl.wypozyczalnia.tim.wypozyczalnia.repositories.CarRentRepository;

import java.util.List;

@Service
public class CarRentServiceImpl implements CarRentService {

    private CarRentRepository carRentRepository;

    @Autowired
    public CarRentServiceImpl(CarRentRepository carRentRepository) {
        this.carRentRepository = carRentRepository;
    }

    // todo: implement functions

    @Override
    public List<CarRent> getCarHires() {
        return null;
    }

    @Override
    public CarRent getCarHire(Long id) {
        return null;
    }

    @Override
    public CarRent createCarHire(CarRent carRent) {
        return null;
    }

    @Override
    public CarRent updateCarHire(Long id, CarRent carRent) {
        return null;
    }
}
