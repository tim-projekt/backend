package pl.wypozyczalnia.tim.wypozyczalnia.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wypozyczalnia.tim.wypozyczalnia.models.CarRent;
import pl.wypozyczalnia.tim.wypozyczalnia.services.CarRentService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class RentCarController {

    private CarRentService carRentService;

    @Autowired
    public RentCarController(CarRentService carRentService) {
        this.carRentService = carRentService;
    }

    // todo: Create CarHireDTO class
    // todo: change return value in methods declaration
    // todo: implement functions and validation
    @GetMapping("/rental-cars")
    public ResponseEntity<List<?>> getCarRents() {

        return new ResponseEntity<>(carRentService.getCarHires(), HttpStatus.OK);
    }

    @PostMapping("/rental-cars")
    public ResponseEntity<?> createCarRent(@Valid @RequestBody CarRent carRent) {

        return new ResponseEntity<>(carRentService.createCarHire(carRent), HttpStatus.OK);
    }

    @GetMapping("/rental-cars/{id}")
    public ResponseEntity<?> getCarRent(@PathVariable Long id) {

        return new ResponseEntity<>(carRentService.getCarHire(id), HttpStatus.OK);
    }

    @PutMapping("/rental-cars/{id}")
    public ResponseEntity<?> updateCarRent(@PathVariable Long id,
                                           @Valid @RequestBody CarRent carRent) {

        return new ResponseEntity<>(carRentService.updateCarHire(id, carRent), HttpStatus.OK);
    }
}
