package pl.wypozyczalnia.tim.wypozyczalnia.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Vehicle;
import pl.wypozyczalnia.tim.wypozyczalnia.services.VehicleService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class VehicleController {

    private VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }


    // todo: Create VehicleDTO class
    // todo: change return value in methods declaration
    // todo: implement functions and validation

    @GetMapping("/vehicles")
    public ResponseEntity<List<?>> getVehicles() {

        return new ResponseEntity<>(vehicleService.getVehicles(), HttpStatus.OK);
    }

    @PostMapping("/vehicles")
    public ResponseEntity<?> createVehicle(@Valid @RequestBody Vehicle vehicle) {

        return new ResponseEntity<>(vehicleService.createVehicle(vehicle), HttpStatus.OK);
    }

    @GetMapping("/vehicles/{id}")
    public ResponseEntity<?> getVehicle(@PathVariable Long id) {

        return new ResponseEntity<>(vehicleService.getVehicle(id), HttpStatus.OK);
    }

    @PutMapping("/vehicles/{id}")
    public ResponseEntity<?> updateVehicle(@PathVariable Long id,
                                           @Valid @RequestBody Vehicle vehicle) {

        return new ResponseEntity<>(vehicleService.updateVehicle(id, vehicle), HttpStatus.OK);
    }
}
