package pl.wypozyczalnia.tim.wypozyczalnia.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wypozyczalnia.tim.wypozyczalnia.models.Report;
import pl.wypozyczalnia.tim.wypozyczalnia.services.ReportService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ReportController {

    private ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    // todo: Create ReportDTO class
    // todo: change return value in methods declaration
    // todo: implement functions and validation

    @GetMapping("/reports")
    public ResponseEntity<List<?>> getReports() {

        return new ResponseEntity<>(reportService.getUserReports(), HttpStatus.OK);
    }

    @PostMapping("/reports")
    public ResponseEntity<?> createReport(@Valid @RequestBody Report report) {

        return new ResponseEntity<>(reportService.createReport(report), HttpStatus.OK);
    }

    @GetMapping("/reports/{id}")
    public ResponseEntity<?> getReport(@PathVariable Long id) {

        return new ResponseEntity<>(reportService.getReport(id), HttpStatus.OK);
    }

    @PutMapping("/reports/{id}")
    public ResponseEntity<?> updateReport(@PathVariable Long id,
                                          @Valid @RequestBody Report report) {

        return new ResponseEntity<>(reportService.updateReport(id, report), HttpStatus.OK);
    }
}
