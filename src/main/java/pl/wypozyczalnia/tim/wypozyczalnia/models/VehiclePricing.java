package pl.wypozyczalnia.tim.wypozyczalnia.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class VehiclePricing {

    @Id
    @GeneratedValue
    private Long id;
    private String vehicleType;
    private Double pricePerKilometer;
    private Double priceStart;
    private Double pricePerMinute;

}
