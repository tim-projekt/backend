package pl.wypozyczalnia.tim.wypozyczalnia.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity
public class CarRent {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private ApplicationUser user;
    @ManyToOne
    private Vehicle car;
    private Date rentStart;
    private Date rentEnd;
    private Double price;
    private Float rating;

}
