package pl.wypozyczalnia.tim.wypozyczalnia.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Date;

@Data
@Entity
public class Vehicle {
    @Id
    @GeneratedValue
    private Long id;
    private String qrCode;
    private String manufacturer;
    private String model;
    private Date yearOfManufacture;
    private String vinNumber;
    private String licensePlate;
    private Double engineCapacity;
    private Double power;
    private String fuelType;
    private Date endOfInsurance;
    private Date endOfTechnicalExamination;
    @ManyToOne
    private VehiclePricing pricing;
}
